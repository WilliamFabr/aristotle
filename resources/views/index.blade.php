@extends('layout.body')

@section('title', 'início')

@section('content')
<section>
  <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab ec-sticky">
    <i class="material-icons">keyboard_arrow_down</i>
  </button>
  <div class="mdl-grid ec-layout_intro">
    <div class="mdl-cell mdl-cell--12-col">
      <div class="ec-spacer"></div>
      <h2 class="ec-title">E-Cart</h2>
    </div>
    <div class="mdl-cell mdl-cell--5-col"></div>
    <div class="mdl-cell mdl-cell--2-col">
      <img class="mdl-img" src="{{ asset('E-cart.png')}}"/>
    </div>
    <div class="mdl-cell mdl-cell--12-col">
      <div class="ec-spacer"></div>
      <p class="ec-important">IDENTIFICAÇÃO ESCOLAR</p>
      <p class="ec-important">Cadastre sua Instituição de Ensino e busque seus direitos.</p>
    </div>
    <div class="mdl-cell mdl-cell--12-col">
      <div class="ec-spacer"></div>
    </div>
  </div>
</section>
<section>
  <div class="mdl-grid ec-layout_about">
    <div class="mdl-cell mdl-cell--12-col ec-spacer_high"></div>
      <div class="mdl-cell mdl-cell--12-col">
      <h2 class="ec-title">Sobre</h2>
      <p class="ec-important">E-Cart é um programa desenvolvido a partir de uma
         ideia formulada no IFC</p>
      <p class="ec-content">O programa desenvolvido por William Fabre e Kauan
        Colares de Borba, orientados pelo Me. Victor Souza e desenvolvido como
        trabalho de conclusão de curso de Técnico em Informática integrado ao
        Ensino médio do instituto Federal Campus Avançado Sombrio (IFC-CAS),
        Surgiu de uma ideia dos discentes, de aprimorarem a identificação de
        alunos nas escolas públicas, o problema foi identificado em um dia onde
        um dos discentes ao pegar o ônibus percebeu que só alguns dos alunos da
        mesma intituição eram comtemplados pela gratuidade no ônibus, como e que
        parece ser uma cena recorrente ao redor do Brasil, a não identificação dos
        direitos por aqueles que podem possuí-los, por isso os Discentes
        decidiram construir um sistema que todos os alunos de escolas públicas
        podem indicar suas intituições de ensino e buscar pelos direitos a
        transporte público, além de auxiliar os colégios na identificação de
        alunos. Esse sistema abrange todas os níveis básicos de educação
        (pré-escola, fundamental e médio).</br>
      </p>
    </div>
    <div class="mdl-cell mdl-cell--12-col ec-spacer_high"></div>
  </div>
</section>
<section>
  <div class="mdl-grid ec-layout_howWorks">
    <div class="mdl-cell mdl-cell--12-col">
      <h2 class="ec-title">Como Funciona?</h2>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
      <img class="mdl-img" src="{{asset('rc-howWorks_pic1.jpg')}}"/>
    </div>
    <div class="mdl-cell mdl-cell--6-col">
      <p class="ec-content">
        <b>layoutretários</b>: vocês cadastram sua instituição através da aba de login,
        ou clicando no botão abaixo:</br>
      </p>
      <p class="ec-content">
        Logo após você preencher o cadastro o sistema irá, verificar os dados e
        conferi-los, em até 5 dias um e-mail será mandado para você dizendo se a
        IE (Instituição de Ensino) foi cadastrada ou não, logo após é só trazer
        os dados e utilizar nosso sistema interno fazendo <b>login</b>.</p>
      <p class="ec-content">
        <b>Alunos e Responsáveis</b>: Peçam para que sua insituição de ensino aplique
      esse sistema, busque direitos municipais para transporte público, ajude na
      sua própria segurança no reconhecimento dos alunos da sua IE através dessa
      carteirinha.</br>
      </p>
    </div>
    <div class="mdl-cell mdl-cell--12-col">
      <h2 class="ec-important">
        OQUE VOCÊ DESEJA FAZER?
      </h2>
    </div>
    <div class="mdl-cell mdl-cell--12-col ec-spacer"></div>
    <div class="mdl-cell mdl-cell--2-col"></div>
    <div class="mdl-cell mdl-cell--3-col">
      <div class="mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title mdl-card--border">
          <h2 class="mdl-card__title-text">Cadastrar Instituição<h2>
        </div>
        <div class="mdl-card__supporting-text">
          <h3 class="ec-important">Se você é funcionário de uma IE,
          Acesse o link embaixo usando esse botão:</h2>
          <button class="mdl-button mdl-js-button mdl-button--raised ec-howWorks_button">
            Cadastre-se
          </button>
        </div>
      </div>
    </div>
    <div class="mdl-cell mdl-cell--2-col"></div>
    <div class="mdl-cell mdl-cell--3-col">
      <div class="mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title mdl-card--border">
          <h2 class="mdl-card__title-text">Indicar Instituição<h2>
        </div>
        <div class="mdl-card__supporting-text">
          <h3 class="ec-important">Para aluno e Pais que tem algum aluno
          inscrito em uma instituição de Ensino, indiquem o sistema:</h2>
          <button class="mdl-button mdl-js-button mdl-button--raised ec-howWorks_button">
            Indique!
          </button>
        </div>
      </div>
    </div>
    <div class="mdl-cell mdl-cell--12-col ec-spacer"></div>
  </div>
</section>
<section>
  <div class="mdl-grid ec-layout_laws">
    <div class="mdl-cell mdl-cell--12-col">
      <h2 class="ec-title">Busque seus Direitos</h2>
    </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--12-col ec-spacer"></div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
      <div class="mdl-cell mdl-cell--3-col">
        <div class="mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand">
            <h2 class="mdl-card__title-text">Update</h2>
          </div>
          <div class="mdl-card__supporting-text">
            Lorem ipsum dolor sit amet, conlayouttetur adipiscing elit.
            Aenan convallis.
          </div>
          <div class="mdl-card__actions mdl-card--border">
            <a class="ec-content_button mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
              View Updates
            </a>
          </div>
        </div>
      </div>
    <div class="mdl-cell mdl-cell--12-col ec-spacer">
    </div>
  </div>
</section>
@endsection
