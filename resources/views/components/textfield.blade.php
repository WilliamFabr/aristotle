<label class="mdl-form__label">{{$label}} :</label>
<div class="mdl-textfield mdl-js-textfield">
  <input class="mdl-textfield__input" type="text" id="{{$var}}" name="{{$var}}">
  <label class="mdl-textfield__label" for="nome">{{$label}}</label>
</div>
