<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect {{$class}}">
  {{$label}}
</button>
