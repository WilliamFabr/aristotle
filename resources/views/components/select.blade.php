<label class="mdl-form__label">{{$label}} :</label>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
  <select class="mdl-textfield__input ec-content_select" id="{{$var}}" name="{{$var}}">
    <option></option>
    {{$slot}}
  </select>
<label class="mdl-textfield__label" for="{{$var}}">{{$label}}</label>
</div>
