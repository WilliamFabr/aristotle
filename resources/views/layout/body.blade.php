<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="{{ asset('favicon.png') }}"/>
    <title>E-Cart | @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/material.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/layout.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500">
  </head>
  <body>
    <div class="mdl-layout mdl-js-layout">
      <header class="mdl-layout__header mdl-layout__header--transparent">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">@yield('title')</span>
          <div class="mdl-layout-spacer"></div>
          <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="">Sobre</a>
            <a class="mdl-navigation__link" href="">Como Funciona?</a>
            <a class="mdl-navigation__link" href="">Busque seus direitos</a>
            <a class="mdl-navigation__link" href="{{ route('usuario.login', null)}}">Login</a>
          </nav>
        </div>
      </header>
      <main class="mdl-layout__content">
        @yield('content')
        <div class="mdl-layout__footer">
          <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col">
              <p class="ec-important">E-Cart</p>
            </div>
          </div>
        </div>
      </main>
    </div>
  </body>
  <script src= "{{ asset('js/material.min.js') }}"></script>
</html>
