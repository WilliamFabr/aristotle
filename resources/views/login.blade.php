@extends('layout.body')

@section('title', 'login')

@section('content')
<section>
<div class="mdl-grid ec-layout_login">
  <div class="mdl-cell mdl-cell--4-col">

  </div>
  <div class="mdl-cell mdl-cell--4-col ec-login">
    <form action="#">
    <div class="mdl-card mdl-cerd mdl-shadow--2dp">
      <div class="mdl-card__title ec-content_centralize">
        <h2 class="ec-title">Login</h2>
      </div>
      <div class="mdl-card__supporting-text">
          <div class="mdl-cell mdl-cell--12-col">
            <label class="mdl-form__label">Nome de Usuário ou Email:</label>
            <div class="mdl-textfield mdl-js-textfield">
              <input class="mdl-textfield__input" type="text" id="nome" name="nome">
              <label class="mdl-textfield__label" for="nome">Nome de Usuário</label>
            </div>
            <div class="ec-spacer"></div>
            <label class="mdl-form__label">Senha:</label>
            <div class="mdl-textfield mdl-js-textfield">
              <input class="mdl-textfield__input" type="password" id="password" name="password">
              <label class="mdl-textfield__label" for="password">Nome de Usuário</label>
            </div>
            <label class="mdl-form__label">
              Não tem conta? <br/>
            </label>
            <label class="mdl-form__label">
              Registre sua IE agora, e cadastre seu acesso. <a href="{{ route('usuario.create', null) }}">aqui</a> <br />
            </label>
            <div class="ec-spacer_high">
            </div>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored ec-layout_login-button">
              Entrar
            </button>
        </div>
      </div>
    </div>
    </form>
  </div>
  <div class="mdl-cell mdl-cell--4-col">

  </div>
</div>
</section>
@endsection
