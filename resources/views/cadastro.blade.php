@extends('layout.body')

@section('title', 'cadastrar')

@section('content')
<section>
<div class="mdl-grid ec-layout_login">
  <div class="mdl-cell mdl-cell--1-col">

  </div>
  <div class="mdl-cell mdl-cell--10-col ec-login">
    <form action="#">
    <div class="mdl-card mdl-cerd mdl-shadow--2dp">
      <div class="mdl-card__title ec-content_centralize">
        <h2 class="ec-title">Cadastro</h2>
      </div>
      <div class="mdl-card__supporting-text ec-layout_login-card">
        <div class="mdl-grid">
          <div class="mdl-cell mdl-cell--6-col">
            <!-- Coluna da esquerda do Cadastro  -->
            <div class="mdl-grid">
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'inst_nome', 'label' => 'Nome da Instituição'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'inst_cnpj', 'label' => 'CNPJ da Instituição'])
                @endcomponent
              </div>
              <div class="mdl-cell--6-col">
                @component('components.textfield', ['var' => 'inst_sigla', 'label' => 'Sigla'])
                @endcomponent
              </div>
              <div class="mdl-cell--6-col">
                @component('components.ufselect')
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.select', ['var' => 'inst_nivel', 'label' => 'Nível'])
                  <option val="Infantil">Infantil</option>
                  <option val="Fundamental">Fundamental</option>
                  <option val="Medio">Médio</option>
                  <option val="Tecnico">Técnico</option>
                  <option val="Superior">Superior</option>
                  <option val="Pos-Especializacao">Pós Graduação / Especialização</option>
                  <option val="Mestrado-Doutorado">Mestrado/ Doutorado</option>
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.select', ['var' => 'inst_tipo', 'label' => 'Tipo'])
                  <option val="Privada">Privada</option>
                  <option val="Federal">Federal</option>
                  <option val="Estadual">Estadual</option>
                  <option val="Municipal">Municipal</option>
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'inst_site', 'label' => 'Site da Instituição'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'inst_phone', 'label' => 'Telefone'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                <label class="mdl-form__label"><b>Endereço</b></label>
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var'=>'inst_cep', 'label' => 'CEP'])
                @endcomponent
              </div>
            </div>
          </div>
          <div class="mdl-cell mdl-cell--6-col">
            <!-- Coluna da direita do Cadastro -->
            <div class="mdl-grid">
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_nome', 'label' => 'Nome do Usuário'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_nasc', 'label' => 'Data de Nascimento'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.select', ['var' => 'user_cargo', 'label' => 'Cargo'])
                  <option val="secretario">Secetrário Escolar</option>
                  <option val="diretor">Diretor Escolar</option>
                  <option val="assisten">Assistente Pedagógico</option>
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_email', 'label' => 'Email'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_conf-email', 'label' => 'Confirmar Email'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_pass', 'label' => 'Senha'])
                @endcomponent
              </div>
              <div class="mdl-cell--12-col">
                @component('components.textfield', ['var' => 'user_conf-pass', 'label' => 'Confirmar Senha'])
                @endcomponent
              </div>
            </div>
          </div>
          <div class="mdl-cell mdl-cell--5-col">

          </div>
          <div class="mdl-cell mdl-cell--2-col">
            @component('components.submit', ['class' => 'blue', 'label' => 'registrar'])
            @endcomponent
          </div>
        </div>
      </div>
      </div>
    </form>
  </div>
  <div class="mdl-cell mdl-cell--1-col">

  </div>
</div>
</section>

@endsection
