<?php

//
//  IMPORTAÇÃO
//  illuminate/http/Request - usado para verificar métodos de recebimento, que
//  não o GET.
//

use illuminate\http\Request;

//
//  ROTAS
//

Route::get('/', function () {
    return view('index');
})->name('index');


//
//  GRUPO DE ROTAS
//  Equivalente ao Route::resource
Route::prefix('user')->group(function(){
  Route::get('/login', 'usuario@index')->name('usuario.login');
  Route::post('/login', 'usuario@store')->name('usuario.store');
  Route::post('/cadastro', 'usuario@register')->name('usuario.register');
  Route::get('/cadastro', 'usuario@create')->name('usuario.create');
  Route::get('/{usuario}', 'usuario@show')->name('usuario.show');
  Route::get('/atualizar/{usuario}', 'usuario@update')->name('usuario.update');
  Route::delete('/excluir/{usuario}', 'usuario@destroy')->name('usuario.destroy');
  Route::get('/editar/{usuario}', 'usuario@edit')->name('usuario.edit');
});

  //
  //  TIPO DE MÉTODOS PHP
  //  GET, POST: padrões para utilização no envio de dados
  //  PUT: utilizada para tratar opções relacionadas com inscrição de dados
  //  DETELE: utilizado para excluir linhas no bd
  // PATCH: utilizado para atualizar dados
  //
