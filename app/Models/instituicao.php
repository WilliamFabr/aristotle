<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instituicao extends Model
{
    //
    //  MODEL instituicao
    //
    protected $filiable = [
      'id',
      'nome',
      'endereco',
      'icone',
      'cnpj',
    ];

    protected $table = "rt_instituicao";//tabela
}
