<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class telefone extends Model
{
    //
    //  TELEFONE GERAL
    //
    protected $filiable = [
      'id',
      'ddd',
      'numero',
      'instituicao'
    ];

    protected $table = "rt_telefone";//tabela
}
