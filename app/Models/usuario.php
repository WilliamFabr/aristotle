<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    //
    //  USER MODEL
    //

    protected $filiable = [
      'id',
      'nome',
      'fone',
      'email',
      'cpf',
      'nascimento',
      'senha',
      'instituicao'
    ];

    protected $table = 'rt_usuario';

    /*function hasmany() {
      return $this->hasMany(AnotherClass::Class, 'foreignkey');
    }*/

    /*function belongsto() {
      return $this->belongsTo(usuario::class, 'foreignkey');
    }*/

}
