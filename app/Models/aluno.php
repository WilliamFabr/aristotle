<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aluno extends Model
{
    //
    //  Construção do modelo do Aluno
    //

    protected $filiable = [
      'id',
      'Nome',
      'Data_nasc',
      'matricula',
      'endereco',
      'foto',
      'instituicao'
    ];

    protected $table = "rt_aluno";
}
