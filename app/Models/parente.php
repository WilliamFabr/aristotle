<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parente extends Model
{
    //
    //  MODELO PARENTE
    //
    protected $filiable = [
      'id',
      'nome',
      'cpf',
      'rg',
      'fone',
      'email',
      'parentesco',
      'nascimento',
      'endereco',
      'aluno'
    ];

    protected $table = "rt_parente";//tabela

}
