<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class usuario extends Controller
{
    public function index()
    {
      return view('login');
    }

    public function register(){
    }

    public function create()
    {
      return view('cadastro');
    }

    public function store(Request $request)
    {
        //$request->input('variavel');
        //return response("created", 201);
    }

    public function show($nome, $senha)
    {
        //code of model;
        $parametros = ['nome'=>$nome, 'senha'=>$senha];
        return view('user', $parametros);
        //compact('nome')
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
